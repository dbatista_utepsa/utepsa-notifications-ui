;(function() {

  angular.module("utepsa-notifications").controller('NotificationsController', NotificationsController);

  NotificationsController.$inject = ['$scope', 'notificationsService'];

  function NotificationsController($scope, notificationsService) {
    $scope.notifications = null;
    getNotificatios();

    function getNotificatios(){
      var promise = notificationsService.getNotifications();

      promise.then(function (result) {
        $scope.notifications = result;
      });
    }
  }

})();