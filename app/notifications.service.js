;(function() {

	angular.module("utepsa-notifications").factory("notificationsService", notificationsService);

	notificationsService.$inject = ['$http'];
	
	function notificationsService($http){
		

		function getNotifications(){
			return $http({
			  method: 'GET',
			  url: 'http://localhost:9090/api/' + 'notifications/',
			  withCredentials: true,
	          headers: {
	          	'Content-Type': 'application/json; charset=utf-8'
	          }
			}).then(function(notifications) {
				console.log(notifications.data);
			  return notifications.data;
			})
			.catch(function(error) {
			  return error;
			});
		}

		var service = {
				getNotifications: getNotifications
		};

		return service;
		
	}

})();