;(function() {

  angular.module("utepsa-notifications",['ngRoute']);
  /**
   * Definition of the main app module and its dependencies
   */
  angular.module("utepsa-notifications").config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider'];

  function config($routeProvider, $locationProvider, $httpProvider, $compileProvider) {

    $locationProvider.html5Mode(false);

    // routes
    $routeProvider
      .when('/login', {
        redirectTo: 'views/login.html'
      })
      .when('/', {
        templateUrl: 'views/notifications.html',
        controller: 'NotificationsController'
      })
      .otherwise({
        redirectTo: '/'
      });

  }

  angular.module("utepsa-notifications").run(run);

  run.$inject = ['$rootScope', '$location'];

  function run($rootScope, $location) {

    // put here everything that you need to run on page load

  }

})();